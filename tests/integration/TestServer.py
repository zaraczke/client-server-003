import json
import subprocess
import unittest
import socket

from tests.fixtures.ClientCommands import ClientCommands


class TestServer(unittest.TestCase):
    """
    Tests server responses by generating emulated client inputs
    """

    def setUp(self):
        """
        Sets up the server variables for connections
        """
        self.ADDRESS = '127.0.0.1'
        self.PORT = 8081

    def test_connection(self):
        """
        Simple server client emulation test
        """
        #  Starts connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command('help')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closes connections and check if was closed properly and was a response from server
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()
        self.assertEqual(len(data_in), 1, 'Should have 1 response!')

    def test_unsigned_user_command(self):
        """
        Tests server response by unsigned in user inputs
        """
        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command('help')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out = ClientCommands.prepare_command('login', username='admin', password='admin')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out = ClientCommands.prepare_command('register', username='test', password='test',
                                                      repeat_password='test')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 3, 'Should have 3 response!')

        for i in data_in:
            loaded_check = json.loads(i)
            self.assertIsInstance(loaded_check, dict, 'Should be a dict!')

        #  Tests help command
        help_check = json.loads(data_in[0])
        self.assertEqual(len(help_check), 4, 'Should have 4 parameters!')
        self.assertTrue('help' in help_check.keys(), 'Should have help!')
        self.assertTrue('login' in help_check.keys(), 'Should have login!')
        self.assertTrue('register' in help_check.keys(), 'Should have register!')
        self.assertTrue('quit' in help_check.keys(), 'Should have quit!')

        #  Tests login command
        login_check = json.loads(data_in[1])
        self.assertEqual(len(login_check), 2, 'Should have 2 properties!')
        self.assertTrue('user' in login_check.keys(), 'Should have user data!')
        self.assertTrue('message' in login_check.keys(), 'Should have message for user!')

        #  Tests register command
        register_check = json.loads(data_in[2])
        self.assertEqual(len(register_check), 2, 'Should have 2 properties!')
        self.assertTrue('user' in register_check.keys(), 'Should have user data!')
        self.assertTrue('message' in register_check.keys(), 'Should have message for user!')

    def test_unsigned_user_input_errors(self):
        """
        Tests server response to unsigned user input errors
        """
        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command('test')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out = ClientCommands.prepare_command('login', username='admin', password='test')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out = ClientCommands.prepare_command('login', username='usher', password='test')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out = ClientCommands.prepare_command('register', username='test', password='test',
                                                      repeat_password='test')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out = ClientCommands.prepare_command('register', username='tester', password='test',
                                                      repeat_password='tester')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 5, 'Should have 5 response!')
        for i in data_in:
            loaded_check = json.loads(i)
            self.assertIsInstance(loaded_check, dict, 'Should be a dict!')
            self.assertEqual(len(loaded_check), 1, 'Should have 1 parameters!')
            self.assertTrue('error' in loaded_check.keys(), 'Should have an error!')

        #  Tests not existing command
        cmd_check = json.loads(data_in[0])
        self.assertEqual(cmd_check['error'], '404, command not found!', 'Should be a 404 message!')

        #  Tests login password error command
        password_check = json.loads(data_in[1])
        self.assertEqual(password_check['error'], 'Failed, password is not correct!',
                         'Should be a failed by password message!')

        #  Tests login username error command
        username_check = json.loads(data_in[2])
        self.assertEqual(username_check['error'], 'Failed, username is not correct or not exist!',
                         'Should be a failed by username message!')

        #  Tests register user exists command
        user_check = json.loads(data_in[3])
        self.assertEqual(user_check['error'], 'Failed, username is already used!',
                         'Should be a username exists message!')

        #  Tests register user exists command
        match_check = json.loads(data_in[4])
        self.assertEqual(match_check['error'], 'Passwords don\'t match up!',
                         'Should be a pass match error message!')

    def test_user_commands(self):
        """
        Tests signed-in user commands
        """

        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command_user('help')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('message', user='test', text='test message')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('readMessages', user='admin')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('readHistory')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('cleanInbox')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('changePassword', password='user')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 6, 'Should have 6 response!')
        for i in data_in:
            if i != '':
                loaded_check = json.loads(i)
                self.assertIsInstance(loaded_check, dict, 'Should be a dict!')

        #  Tests help command response
        help_check = json.loads(data_in[0])
        self.assertGreaterEqual(len(help_check), 9, 'user should have 9 or more commands!')

        #  Tests message command response
        message_check = json.loads(data_in[1])
        self.assertTrue('message' in message_check.keys(), 'Should contain a message!')
        self.assertEqual(message_check['message'], 'Message successfully sent!',
                         'Server info should contain a success message!')

        # Tests readMessages command response
        r_message_check = json.loads(data_in[2])
        self.assertGreaterEqual(len(r_message_check), 1, 'Uptime should contains 1 key!')
        self.assertTrue('messages' in r_message_check.keys(), 'Should contains a list of messages from a user!')
        self.assertIsInstance(r_message_check['messages'], list, 'Should be a list of messages!')
        self.assertGreaterEqual(len(r_message_check['messages']), 0, 'The list should be of len 0 or greater!')

        # Tests readHistory command response
        history_check = json.loads(data_in[3])
        self.assertEqual(len(history_check), 2, 'History should contains 2 keys!')
        self.assertTrue('inbound' in history_check.keys(), 'Should contains a list of received messages!')
        self.assertTrue('outbound' in history_check.keys(), 'Should contains a list of sent messages!')
        self.assertGreaterEqual(len(history_check['inbound']), 0, 'The len should be 0 or more!')
        self.assertGreaterEqual(len(history_check['outbound']), 0, 'The len should be 0 or more!')

        # Tests cleanInbox command response
        clean_check = json.loads(data_in[4])
        self.assertEqual(len(clean_check), 1, 'Should contains 1 keys!')
        self.assertTrue('message' in clean_check.keys(), 'Should contains a messages!')
        self.assertEqual(clean_check['message'], 'Inbox successfully cleaned!',
                         'Should have a success message!')

        # Tests changePassword command response
        c_pass_check = json.loads(data_in[5])
        self.assertEqual(len(c_pass_check), 1, 'History should contains 1 keys!')
        self.assertTrue('message' in c_pass_check.keys(), 'Should contains a messages!')
        self.assertEqual(c_pass_check['message'], 'User data successfully changed!',
                         'Should have a success message!')

    def test_user_input_errors(self):
        """
        Tests signed-in user input errors
        """

        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command_user('message', user='testers', text='test message')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('message', user='admin',
                                                                 text='2222222222222222222222222\n' +
                                                                 '222222222222222222222222\n' +
                                                                 '2222222222222222222222222\n' +
                                                                 '2222222222222222222222222\n' +
                                                                 '2222222222222222222222222\n' +
                                                                 '2222222222222222222222222\n' +
                                                                 '2222222222222222222222222\n' +
                                                                 '2222222222222222222222222\n' +
                                                                 '2222222222222222222222222\n' +
                                                                 '2222222222222222222222222\n' +
                                                                 '2222222222222222222222222'
                                                                 )
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 2, 'Should have 2 response!')

        for i in data_in:
            loaded_check = json.loads(i)
            self.assertIsInstance(loaded_check, dict, 'Should be a dict!')

        #  Tests for message user inbox command errors
        inbox_check = json.loads(data_in[0])
        self.assertTrue('error' in inbox_check.keys(), 'Should  have an error message!')
        self.assertEqual(inbox_check['error'], 'Failed, user don\'t exists',
                         'Should have a failed by username error message')

        #  Tests for message text len command errors
        len_check = json.loads(data_in[1])
        self.assertTrue('error' in len_check.keys(), 'Should  have an error message!')
        self.assertEqual(len_check['error'], 'Failed, text is to long,  max length is 255',
                         'Should have a failed by communicate length error message')

    def test_admin_commands(self):
        """
        Tests admin no data commands
        """

        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command_admin('help')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_admin('info')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_admin('uptime')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_admin('communicate', text='tests')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 4, 'Should have 4 response!')

        for i in data_in:
            loaded_check = json.loads(i)
            self.assertIsInstance(loaded_check, dict, 'Should be a dict!')

        #  Tests help command response
        help_check = json.loads(data_in[0])
        self.assertGreaterEqual(len(help_check), 20, 'Admin should have 20 or more commands!')

        #  Tests info command response
        info_check = json.loads(data_in[1])
        self.assertGreaterEqual(len(info_check), 2, 'Server info should contains 2 or more data info!')
        self.assertTrue('server_version' in info_check.keys(), 'Server info should contain a server version!')
        self.assertTrue('server_created_at' in info_check.keys(),
                        'Server info should contain a server creation time!')

        # Tests uptime command response
        uptime_check = json.loads(data_in[2])
        self.assertEqual(len(uptime_check), 1, 'Uptime should contains only 1 info!')
        self.assertTrue('server_life' in uptime_check.keys(), 'Should contains server life time!')

        # Tests uptime command response
        communicate_check = json.loads(data_in[3])
        self.assertEqual(len(communicate_check), 2, 'Communicate should contains a message and a list!')
        self.assertTrue('message' in communicate_check.keys(), 'Should contains a message!')
        self.assertTrue('unsent_to_users' in communicate_check.keys(),
                        'Should contains a list of user who don\'t received the message!')
        self.assertGreaterEqual(len(communicate_check['unsent_to_users']), 0, 'The len should be 0 or more!')
        self.assertEqual(communicate_check['message'], 'Message successfully sent!', 'Should have success message!')

    def test_admin_data_get_commands(self):
        """
        Tests admin data get commands
        """

        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command_admin('getUsers')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_admin('getMessages')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_admin('getAuths')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 3, 'Should have 3 response!')

        for i in data_in:
            loaded_check = json.loads(i)
            self.assertIsInstance(loaded_check, list, 'Should be a list!')
            self.assertGreaterEqual(len(loaded_check), 1, 'Should be 1 or more objects!')

    def test_admin_data_add_commands(self):
        """
        Tests admin data add commands
        """

        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command_admin('addUser', username='tester',
                                                                  password='tester', authorization='user')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 1, 'Should have 1 response!')

        for i in data_in:
            loaded_check = json.loads(i)
            self.assertIsInstance(loaded_check, dict, 'Should be a dict!')
            self.assertTrue('message' in loaded_check.keys(), 'Should  have a message!')

        #  Tests for addUser response
        a_user_check = json.loads(data_in[0])
        self.assertEqual(a_user_check['message'], 'User successfully added!')

    def test_admin_data_modification_commands(self):
        """
        Tests admin data modification commands
        """

        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out = ClientCommands.prepare_command_admin('changeUser',
                                                            user_id=1,
                                                            username='',
                                                            password='',
                                                            authorization='user')
            s.send(json.dumps(data_out).encode())
            res: str = s.recv(2048).decode()
            data_in.append(res)
            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 1, 'Should have 1 response!')

        #  Tests for changeUser response
        c_user_check = json.loads(data_in[0])
        self.assertIsInstance(c_user_check, dict, 'Should be a dict!')
        self.assertTrue('message' in c_user_check.keys(), 'Should have a message!')
        self.assertEqual(c_user_check['message'], 'User data successfully changed!')

    def test_admin_data_deletion_commands(self):
        """
        Tests admin data deletion commands
        """

        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command_admin('deleteUser', user_id=4)
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_admin('deleteUser', user_id=3)
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_admin('deleteMessage', message_id=12)
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 3, 'Should have 3 response!')

        for i in data_in:
            loaded_check = json.loads(i)
            self.assertIsInstance(loaded_check, dict, 'Should be a dict!')
            self.assertTrue('message' in loaded_check.keys(), 'Should  have a message!')

        #  Tests for deleteMessage response
        d_message_check = json.loads(data_in[2])
        self.assertEqual(d_message_check['message'], 'Message successfully Deleted!')

        #  Tests for deleteUser response
        d_user_check = json.loads(data_in[1])
        self.assertEqual(d_user_check['message'], 'User successfully Deleted!')

    def test_admin_input_errors(self):
        """
        Tests admin input errors
        """

        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command_admin('addUser', username='admin', password='user',
                                                                  authorization='user')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_admin('communicate',
                                                                  text='2222222222222222222222222\n' +
                                                                  '222222222222222222222222\n' +
                                                                  '2222222222222222222222222\n' +
                                                                  '2222222222222222222222222\n' +
                                                                  '2222222222222222222222222\n' +
                                                                  '2222222222222222222222222\n' +
                                                                  '2222222222222222222222222\n' +
                                                                  '2222222222222222222222222\n' +
                                                                  '2222222222222222222222222\n' +
                                                                  '2222222222222222222222222\n' +
                                                                  '2222222222222222222222222'
                                                                  )
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 2, 'Should have 2 response!')
        for i in data_in:
            loaded_check = json.loads(i)
            self.assertIsInstance(loaded_check, dict, 'Should be a dict!')
            self.assertTrue('error' in loaded_check.keys(), 'Should  have an error message!')

        # Tests for addUser command errors
        add_check = json.loads(data_in[1])
        self.assertEqual(add_check['error'], 'Failed, text is to long,  max length is 255!',
                         'Should have a failed by communicate length error message')

        #  Tests for communicate command errors
        communicate_check = json.loads(data_in[0])
        self.assertEqual(communicate_check['error'], 'Failed, username is already used!',
                         'Should have a failed by username error message')

    def test_unauthorized_commands(self):
        """
        Test calling an unauthorized command
        """

        #  Starts all connections
        p = subprocess.Popen('python ././server.py')
        data_in: list = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.ADDRESS, self.PORT))

            #  Preparing data to check
            data_out: dict = ClientCommands.prepare_command_user('info')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('uptime')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('communicate', text='tests')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('addUser', username='tester',
                                                                 password='tester',
                                                                 authorization='user')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('changeUser', user_id='4',
                                                                 username='test')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('deleteUser', user_id='4')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('deleteUser', user_id='3')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('deleteMessage', user_id='13')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('getUsers')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('getMessages')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command_user('getAuths')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('changePassword', password='tester')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('cleanInbox')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('message', user='user', text='tests')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('readMessages', user='user')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())
            data_out: dict = ClientCommands.prepare_command('readHistory')
            s.send(json.dumps(data_out).encode())
            data_in.append(s.recv(2048).decode())

            data_out = ClientCommands.prepare_command('quit')
            s.send(json.dumps(data_out).encode())

        #  Closing connection and test that connection is closed
        p.kill()
        p.wait()
        self.assertRaises(OSError, s.recv, 2048)
        s.close()

        #  Tests if commands have a response
        self.assertEqual(len(data_in), 16, 'Should have 16 response!')

        #  Tests responses
        for i in data_in:
            loaded_data = json.loads(i)
            self.assertIsInstance(loaded_data, dict)
            self.assertTrue('error' in loaded_data.keys(), 'Should have an error message!')
            self.assertEqual(loaded_data['error'], '404, command not found!',
                             'Should have a 404 message!')


if __name__ == '__main__':
    unittest.main()
